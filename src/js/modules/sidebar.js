const sidebarLink = $(".sidebar-item-link");
const hideSideBar = $("#sidebar-hide-btn");

$(sidebarLink).click(function () {
    const container = $(this).parents('.sidebar__list-item')
    if ($(this).attr('aria-expanded')) {
        container.toggleClass('is-active-bg-color');
    }
    if ( $('body').hasClass('sidebar-is-hidden') ) {
        $('body').removeClass("sidebar-is-hidden");
        $("#sidebar").animate({
            width: "225px"
        }, 500);
        $(".header").animate({
            width: "calc(100% - 70px)"
        }, 500);
        $(".sidebar__list-content").animate({
            opacity: "1"
        }, 500);
    }
});

$(hideSideBar).click(function () {
    $('body').toggleClass("sidebar-is-hidden");
    if ( $('body').hasClass("sidebar-is-hidden") ) {
        // $('.sidebar__list-item').removeClass('is-active-bg-color')
        // $('.sidebar-item-link').collapse()
        $("#sidebar").animate({
            width: "70px"
        }, 500, function () {
            // Animation complete.
        });
        $(".header").animate({
            width: "calc(100% - 225px)"
        }, 500);
        $(".sidebar__list-content").animate({
            opacity: "0"
        }, 500);
    } else if ( !$('body').hasClass("sidebar-is-hidden") ) {
        $("#sidebar").animate({
            width: "225px"
        }, 500);
        $(".header").animate({
            width: "calc(100% - 70px)"
        }, 500);
        $(".sidebar__list-content").animate({
            opacity: "1"
        }, 500);
    }

});


