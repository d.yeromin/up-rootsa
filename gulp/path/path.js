const path = {
  src: {
    html:      'src/templates/*.html',
    htmlWatch: 'src/templates/**/*.html',
    style:     'src/style/*.{scss,sass}',
    js:        'src/js/*.js',
    jsWatch:   'src/js/**/*.js',
    img:       ['src/img/**/*.*', '!src/img/sprite/**/*.*'], // all images but not sprites
    sprite:    'src/img/sprite/*.*',
    spriteCss: 'src/style/sprite',
    php:       'src/**/*.php',
    fonts:     "src/fonts/**/*.*",
    data:      "./src/data.json",
    vendor:    'src/vendor/**/*.*',
    data:      './src/data.json'
  },
  dev: {
    root:      'dev/',
    style:     'dev/style',
    js:        'dev/js',
    img:       'dev/img',
    sprite:    'dev/img/sprite',
    vendor:    'dev/vendor',
    fonts:     'dev/fonts/',
    htmlWatch: 'dev/*.html',
  },
  prod: {
    root:      'prod/',
    style:     'prod/style',
    js:        'prod/js',
    img:       'prod/img',
    sprite:    'prod/img/sprite',
    vendor:    'prod/vendor',
    fonts:     'prod/fonts',
  }
};

module.exports = path;