<!-- https://gist.github.com/yeromin/12f7f13e6bea8b8392d0a0ce779a5ce6 -->
<!-- https://commonmark.org/help/ -->

<!-- 
```
login modal/
├── scss
│   └── custom.scss
└── node_modules/
    └── bootstrap
        ├── js
        └── scss
``` 
-->

# Components Description

## Project classes definition

1.The first block class is custom class to modify Bootstrap styles. For example:

```html
<div class="login-modal row position-fixed"></div>
```

`.login-modal` is custom class to apply custom styles.

---
2.Second list of block's classes define bootstrap-grid.

```html
<div class="login-modal__box col-12 col-xs-12 col-md-6 col-lg-3 rounded" tabindex="-1" role="dialog"></div>
```

`col-12 col-xs-12 col-md-6 col-lg-3` - it's Bootstrap's grid options.

---
3.The last of CSS-classes list define style property for current element.

```html
<div class="login-modal__box col-12 col-xs-12 col-md-6 col-lg-3 rounded px-0 " tabindex="-1" role="dialog"></div>
```

`rounded px-0` - define how the element looks.

---
---

## Pages

1. [Login](/login.html)

2. [Dashboard](/login.html)

### Components List

#### Login

```txt
Components/
└── login-modal.html
    └── input.html
```

`.modal-bg-overlay` - class for empty block to apply overlay

---
---
---
